package android.mobilestudio.gpstracker;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by pisoo on 8/14/2017.
 */

public class LocationController extends Service implements LocationListener {

    LocationUpdate locationUpdate;
    Context mContext;
    LocationAlerts alerts;
    Location_ location;
    Boolean initialLocation = true;

    public LocationController(LocationUpdate locationUpdate, Context context) {
        this.locationUpdate = locationUpdate;

        mContext = context;
        alerts = new LocationalertsImpl();
        location = new Location_();
        if (location.getLocation(this, context) == null) {
            alerts.showSettingsAlert(context);
        }
    }

    public void setMinDistanceChangeForUpdates(long minDistanceChangeForUpdates) {
        location.setMinDistanceChangeForUpdates(minDistanceChangeForUpdates);
    }

    public void setMinTimeBwUpdates(long minTimeBwUpdates) {
        location.setMinTimeBwUpdates(minTimeBwUpdates);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (initialLocation) {
            locationUpdate.onLocationInit(location);
            initialLocation = false;
        } else {
            locationUpdate.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        locationUpdate.onStatusChanged(provider, status, extras);
    }

    @Override
    public void onProviderEnabled(String provider) {
        locationUpdate.onProviderEnabled(provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        locationUpdate.onProviderEnabled(provider);
    }

}
