package android.mobilestudio.gpstracker;

import android.content.Context;

/**
 * Created by pisoo on 8/14/2017.
 */

public interface LocationAlerts  {
   void showSettingsAlert (Context context) ;
}
