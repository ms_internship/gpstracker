package android.mobilestudio.gpstracker;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by pisoo on 8/14/2017.
 */

public class Location_ {
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location current; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static long MIN_TIME_BW_UPDATES = 1000 * 1 * 1; // 1 minute

    // Declaring a Location_ Manager
    private LocationManager locationManager;


    public Location getLocation(LocationListener listener, final Context mContext) {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(mContext.LOCATION_SERVICE);
            //showSettingsAlert(mContext);
            // getting GPS status

            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            Location location = getLastKnownLocation();
            if(location ==null){
                Log.v("hey ", "hey");
            }

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                // showSettingsAlert() ;
                return null;
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                //OR if GPS Enabled get lat/long using GPS Services
                if (isNetworkEnabled || (isGPSEnabled && current != null)) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);
                    if (locationManager != null) {
                        current = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (current != null) {
                            latitude = current.getLatitude();
                            longitude = current.getLongitude();
                        }
                    }
                }
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return current;
    }

    public static long getMinDistanceChangeForUpdates() {
        return MIN_DISTANCE_CHANGE_FOR_UPDATES;
    }

    public static long getMinTimeBwUpdates() {
        return MIN_TIME_BW_UPDATES;
    }

    public void setMinDistanceChangeForUpdates(long minDistanceChangeForUpdates) {
        MIN_DISTANCE_CHANGE_FOR_UPDATES = minDistanceChangeForUpdates;
    }

    public void setMinTimeBwUpdates(long minTimeBwUpdates) {
        MIN_TIME_BW_UPDATES = minTimeBwUpdates;
    }


    public void showSettingsAlert(final Context mContext) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    private Location getLastKnownLocation() {
        try {
            List<String> providers = locationManager.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                Location l = locationManager.getLastKnownLocation(provider);


                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {
                    //Log.d("found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            if (bestLocation == null) {
                return null;
            }
            return bestLocation;
        }
        catch (SecurityException e )
        {
             return  null ;
        }

    }



}
